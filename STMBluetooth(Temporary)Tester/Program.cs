﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WAADInterface;
using WAADInterface.Bluetooth_classes;
using Windows.Devices.Bluetooth;
using Windows.Devices.Bluetooth.GenericAttributeProfile;
using Windows.Devices.Enumeration;

namespace STMBluetooth_Temporary_Tester
{
    class Program
    {
        static  void  Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            var tcs = new TaskCompletionSource<bool>();

            Task.Run(async () =>
            {
            try
            {



                //New watcher
                var watcher = new STMBluetoothLEAdvertisementWatcher(new GATTServicesIds());


                watcher.StartedListening += () =>
                {
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.WriteLine("Started listening");
                };


                watcher.PairedComplete += (device) =>
                 {
                     Console.ForegroundColor = ConsoleColor.Blue;
                     Console.WriteLine($"Paired complete with {device}");
                 };

                watcher.StoppedListening += () =>
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Fuck off, listening");
                };

                watcher.NewDeviceDiscovered += (device) =>
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"New device: {device}");
                };
                watcher.DeviceNameChanged += (device) =>
                {
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    Console.WriteLine($"Device name was changed: {device}");
                };



                watcher.DeviceTimeout += (device) =>
                {
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine($"Device was removed 'cause timeout: {device}");
                };

                watcher.LastValues += (mServicesUpdatedValues, Guid) =>
                {
                    
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine($"Last values from service with UUID {Guid}, value:{mServicesUpdatedValues[Guid].Item1}, timestamp:{mServicesUpdatedValues[Guid].Item2} ");


                };

                //New reader
                STMBluetoothLEDataReader mReader = new STMBluetoothLEDataReader();

                    mReader.LookAtLastReadCharacteristic += (dataToValue) =>
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.WriteLine($"Last values from read service:{dataToValue}");


                    };

                    //Testing listenings
            watcher.StartListening();



                    while (true)
                    {
                        //Press enter for show list of devices
                        var command = Console.ReadLine()?.ToLower().Trim();
                        if (string.IsNullOrEmpty(command))
                        {

                            var devices = watcher.DiscoveredDevices;
                            Console.ForegroundColor = ConsoleColor.Magenta;
                            Console.WriteLine($"We currently have { devices.Count} devices");

                            foreach (var device in devices)
                            {
                                
                                string deviceDataPrint = String.Format("Name:\t{0},\nAddress:\t{1},\nDeviceID:\t{2},\n", device.Name, device.Address, device.DeviceId);
                                Console.WriteLine(deviceDataPrint);
                            }

                        }
                        else if (command == "c")
                        {

                            var WAADevice = watcher.DiscoveredDevices.FirstOrDefault(f => f.Name.ToLower().Contains("bluetooth"));
                            if (WAADevice == null)
                            {
                                Console.WriteLine("No WAADevice found...");
                                continue;
                            }
                            Console.WriteLine("Connecting to WAADevice...");
                            try
                            {
                                string servicesPrint = null;
                                GattDeviceServicesResult gattResult = null;
                                BluetoothConnectionStatus connectionStatus = await watcher.ConnectToDeviceAsync(WAADevice.DeviceId);

                                if(connectionStatus == BluetoothConnectionStatus.Connected)
                                    gattResult = await watcher.GetDeviceGattServices(WAADevice.DeviceId);

                                if (gattResult.Status == GattCommunicationStatus.Success)
                                {
                                    servicesPrint = String.Format("Discovered services:\t{0}\n", gattResult.Services);
                                    foreach (GattDeviceService service in gattResult.Services) {
                                        Console.WriteLine(service.Uuid);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("Failed to pair to Countour..");
                                Console.WriteLine(ex);
                            }
                        }
                        else if (command == "q") {
                            break;
                        }
                    }
              
                    tcs.TrySetResult(true);
                }
                finally { 
                
                ///if something goes wrong
                tcs.TrySetResult(false);

                }
            });

            tcs.Task.Wait();

        }
    }
}
