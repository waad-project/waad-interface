﻿using System;
using System.Diagnostics;
using System.IO.Ports;
using System.Security.Cryptography.X509Certificates;

namespace SprawdzenieCOM
{
    class Program
    {
        static void Main(string[] args)
        {
            Test test = new Test();
            Stopwatch stopWatch = new Stopwatch();
            

            test.openSerial();
            Console.WriteLine("start");
            while(true)
            {
                stopWatch.Start();
                test.writeSerial();
                while (test.Receiving()) { };
                test.cnt = 0;
                stopWatch.Stop();
                // Get the elapsed time as a TimeSpan value.
                TimeSpan ts = stopWatch.Elapsed;
                string elapsedTime = String.Format("{0:00}.{1}", ts.Seconds, ts.Milliseconds);
                Console.WriteLine("RunTime " + elapsedTime);

            }
            
        }

    }
}
