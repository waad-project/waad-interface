﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Text;

namespace SprawdzenieCOM
{
    class Test
    {

        SerialPort serialPort;
        char[] receive;
        public long cnt;
        public Test()
        {
            this.serialPort = new SerialPort();
            this.receive = new char[100000];
            this.cnt = 0;
            this.serialPort.DataReceived += SerialPort_DataReceived;
            string[] x = SerialPort.GetPortNames();
            foreach (var n in x)
            {
                Console.WriteLine(n);
            }
        }

        public void openSerial()
        {
            this.serialPort.PortName = "COM5";
            this.serialPort.BaudRate = 921600;
            this.serialPort.Open();
        }

        public void writeSerial()
        {
            char[] data = new char[100000];
            for (int i = 0; i < 100000; i++)
            {
                data[i] = 'c';
            }
            serialPort.Write(data, 0, 100000);
            serialPort.DataReceived += SerialPort_DataReceived;
        }
        private void SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            this.cnt += this.serialPort.Read(this.receive, 0, 100000);
            Console.WriteLine(cnt.ToString());
        }

        public bool Receiving()
        {
            return !(cnt >= 100000);
        }
    }
}
