﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static WAADInterface.Bluetooth_classes.COMLIB.COM_Enum;


namespace WAADInterface.Bluetooth_classes.COMLIB
{
    class ADCLib
    {
        /// <summary>
        /// Enum types used in this class
        /// </summary>
        #region Enums
        public enum RUN_STATES
        {
            STOPPED,
            RUNNING
        }
        public enum PAUSE_STATES
        {
            NOT_PAUSED,
            PAUSED
        }
        #endregion
        #region Properties

        #region Lists and thier properties
        public List<int> FrequenciesAvailable;
        public List<byte> ChannelsList;
        public List<int> AdcDataValues;
        public List<int> ActiveChannelIndexes;
        public int ActiveChannelsNumber;

        #endregion
        public MainWindow window;

        public int dataLength;

        public int datasimpleDataValuePosition;

        public RUN_STATES runStates;

        public PAUSE_STATES pauseStates;

        public int sDCardresolution;

        public int numberOfSingleChannelConversion;

       

        public bool channelsSet = false;

        public volatile byte freqValue;

        #endregion
        #region data properties
        public int simpleDataValuePosition;
        #endregion
        #region Constructor

        public ADCLib(MainWindow _window)
        {
            runStates = RUN_STATES.STOPPED;
            pauseStates = PAUSE_STATES.NOT_PAUSED;
            window = _window;
            ActiveChannelsNumber = 0;
            dataLength = 128;
            simpleDataValuePosition = 5;
            FrequenciesAvailable = new List<int>();
            ChannelsList = new List<byte>((Enumerable.Repeat((byte)0, 8)));
            AdcDataValues = new List<int>();
            ActiveChannelIndexes = new List<int>();
          
            sDCardresolution = 0;
            

        }

        #endregion
        #region setters
        public void setRunState(byte stateStatus) => this.runStates =  stateStatus == 1 ? RUN_STATES.RUNNING : RUN_STATES.STOPPED;

        public void setAdcData(byte[] frame)
        {
            int total = dataLength /ActiveChannelsNumber;
            total *=ActiveChannelsNumber;
            int[] buffer = new int[total];
            for (int i = 0; i < buffer.Length; i++) buffer[i] = frame[i * 2 + (byte)BASIC_FRAME_OFFSETS.RESPONSE_DATA_OFFSET] * 64 + frame[i * 2 + 1 + (byte)BASIC_FRAME_OFFSETS.RESPONSE_DATA_OFFSET];

            for (int i = 0; i < buffer.Length;)
            {
                for (int j = 0; j < ActiveChannelsNumber; j++)
                {
                    window.Dispatcher.Invoke(() => window.AddPoint(buffer[i], ActiveChannelIndexes[j]));
                    i++;
                    if (i >= buffer.Length) break;
                }
            }

        }

        internal void setFrequencyFromResponse(byte[] freqValue)
        {
            this.freqValue = freqValue[simpleDataValuePosition];

        }

        internal void setFrequencyFromGUI(byte frequencyValue)
        {
            freqValue = frequencyValue;
        }

        internal void setAvailableFrequencies(byte[] frame)
        {
            FrequenciesAvailable.Clear();
            for (int i = simpleDataValuePosition; i < frame.Length - 1; i = i + 2)
            {
                FrequenciesAvailable.Add(frame[i] * (int)Math.Pow(10, frame[i + 1]));
            }

        }

        internal void setChannelsActiveFromResponse(byte[] frame)
        {
            ActiveChannelIndexes.Clear();
            for (int i = simpleDataValuePosition; i < frame.Length - 1; ++i)
            {
                ChannelsList[i - simpleDataValuePosition] = (frame[i]);

            }

            ActiveChannelsNumber = ChannelsList.FindAll(findChannelsList).Count;

            for (int i = 0; i < ChannelsList.Count; ++i)
            {
                if (ChannelsList[i] == 1) ActiveChannelIndexes.Add(i);
            }


        }

        internal void setChannelsActiveFromGUI(byte[] guiArray)
        {
            ActiveChannelIndexes.Clear();
            for (int i = 0; i < guiArray.Length; ++i)
            {
                ChannelsList[i] = (guiArray[i]);

            }

            ActiveChannelsNumber = ChannelsList.FindAll(findChannelsList).Count;

            for (int i = 0; i < ChannelsList.Count; ++i)
            {
                if (ChannelsList[i] == 1) ActiveChannelIndexes.Add(i);
            }
        }

        private bool findChannelsList(byte status)
        {
            if (status == 1) return true;
            return false;
        }
        #endregion
    }
}



