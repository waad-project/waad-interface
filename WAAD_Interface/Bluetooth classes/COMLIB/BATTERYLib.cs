﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WAADInterface.Bluetooth_classes.COMLIB
{
    class BATTERYLib
    {
        #region links
        //TOFIX: This class should contain parsed value not raw bytes
        // href="https://github.com/sputnikdev/bluetooth-gatt-parser/blob/master/src/main/resources/gatt/characteristic/org.bluetooth.characteristic.battery_level.xml"
        //TOFIX: Should be enum type
        // href="https://github.com/sputnikdev/bluetooth-gatt-parser/blob/master/src/main/resources/gatt/characteristic/org.bluetooth.characteristic.battery_power_state.xml"

        #endregion

        public int battery_level;

        #region enums
        public enum POWER_STATES
        {
            PLUGGED_IN,
            NOT_PLUGGED_IN
        }
        #endregion
        #region properties
        public POWER_STATES battery_power_state;
        private int datasimpleDataValuePosition;
        #endregion
        #region Constructor
        public BATTERYLib()
        {
            datasimpleDataValuePosition = 5;
            battery_power_state = POWER_STATES.NOT_PLUGGED_IN;
            battery_level = 0;

        }
        #endregion

        #region setters
        internal void setChargeState(byte[] frame)
        {
            if (frame[datasimpleDataValuePosition] == 1)
            {
                this.battery_power_state = POWER_STATES.PLUGGED_IN;
            }
            else
            {
                this.battery_power_state = POWER_STATES.NOT_PLUGGED_IN;
            }

        }
        internal void setPowerLevel(byte[] frame)
        {

            this.battery_level = frame[datasimpleDataValuePosition] * 16 + frame[datasimpleDataValuePosition + 1];


        }

        #endregion



    }

}
