﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Text;
using static WAADInterface.Bluetooth_classes.COMLIB.COM_Enum;
using static WAADInterface.Bluetooth_classes.COMLIB.COM_ApiValidator;
using System.Linq;
using System.Threading;
using Windows.Devices.Adc;
using static WAADInterface.Bluetooth_classes.COMLIB.ADCLib;
using System.Collections.Concurrent;

namespace WAADInterface.Bluetooth_classes.COMLIB
{
    class COM_ApiHandler
    {
        #region Properties
        #region Tools
        public SerialPort serialPort;
        public ADCLib adcHandler;
        public BATTERYLib batteryHandler;
        public SDCARDLib sdCardHandler;
        public MainWindow window;
        #endregion

        #region Control&Logic
        public bool connected { get; set; }
        public bool running { get; private set; }
        public bool dataReceived { get; private set; }
        public bool manualTriggered { get; private set; }
        public PENDING_FLAGS pendingAction { get; private set; }
        //to remove
        public long count = 0;
        public WRITE_STATUS writeStatus { get; private set; }
        public byte pendingService { get; private set; }
        public byte pendingCharacteristic { get; private set; }
        public byte pendingResponseType { get; private set; }
        public NOTIFY_FLAGS notifyReceived { get; private set; }
        public object locker;
        #endregion

        #region DataFlow
        public List<byte> rawData;
        public ConcurrentQueue<byte[]> frameData;
        #endregion

        #region Resources

        public string[] availableComsName;
        public string usedComName;

        internal Database DataStorage;
        #endregion
        #endregion

        #region Events
        public event Action<string[]> NewPortDiscovered = (availableComsName) => { };
        public event Action<bool> PortConnectionHasBeenMade = (connectionFlag) => { };
        public event Action<bool> BluetoothConnectionHasBeenMade = (connectionFlag) => { };
        public event Action<int> BluetoothBatteryPowerLevel = (powerLevel) => { };
        public event Action<int> BluetoothBatteryChargeState = (chargeState) => { };
        public event Action<List<int>> BluetoothAvaiableFreq = (hzList) => { };
        #endregion

        #region WindowInvokes
        private void COMLib_BluetoothAvaiableFreq(List<int> obj) => window.Dispatcher.Invoke(() => window.addAvHz(obj));
        private void COMLib_BluetoothBatteryChargeState(int obj) => window.Dispatcher.Invoke(() => window.updateBatteryChargeState(obj));
        private void COMLib_BluetoothBatteryPowerLevel(int obj) => window.Dispatcher.Invoke(() => window.updateBatteryLevel(obj));
        private void COMLib_BluetoothConnectionHasBeenMade(bool obj) => window.Dispatcher.Invoke(() => window.informAboutBluetoothConnection(obj));
        private void COMLib_PortConnectionHasBeenMade(bool obj) => window.Dispatcher.Invoke(() => window.informAboutPortConnection(obj));
        private void COMLib_NewPortDiscovered(string[] obj) => window.Dispatcher.Invoke(() => window.createItemInBox(obj));
        #endregion

        public COM_ApiHandler(MainWindow _window)
        {
            window = _window;
            serialPort = new SerialPort();
            adcHandler = new ADCLib(window);
            batteryHandler = new BATTERYLib();
            sdCardHandler = new SDCARDLib();
            locker = new object();
            connected = false;
            running = false;
            dataReceived = false;
            manualTriggered = false;
            pendingAction = PENDING_FLAGS.PENDING_FOR_CONNECTION;
            pendingService = 0;
            pendingCharacteristic = 0;
            pendingResponseType = 0;
            notifyReceived = NOTIFY_FLAGS.NO_NOTIFY;
            rawData = new List<byte>();
            frameData = new ConcurrentQueue<byte[]>();

            DataStorage = new Database("sqlite.db");
            DataStorage.Initialize();

            serialPort.DataReceived += SerialPort_DataReceived;
            NewPortDiscovered += COMLib_NewPortDiscovered;
            PortConnectionHasBeenMade += COMLib_PortConnectionHasBeenMade;
            BluetoothConnectionHasBeenMade += COMLib_BluetoothConnectionHasBeenMade;
            BluetoothBatteryPowerLevel += COMLib_BluetoothBatteryPowerLevel;
            BluetoothBatteryChargeState += COMLib_BluetoothBatteryChargeState;
            BluetoothAvaiableFreq += COMLib_BluetoothAvaiableFreq;
        }

        #region SerialPort
        public void SerialPort_Open()
        {

            serialPort.PortName = usedComName;
            serialPort.BaudRate = 921600;
            this.serialPort.Open();
            PortConnectionHasBeenMade(true);
        }
        private void SerialPort_DataWrite(byte[] frame) => serialPort.Write(frame, 0, frame.Length);
        private void SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            lock (locker) 
            {
                int bytesToRead = serialPort.BytesToRead;
                byte[] _ = new byte[bytesToRead];
                serialPort.Read(_, 0, bytesToRead);
                rawData.AddRange(_);
                dataReceived = true;
            }
        }
        #endregion

        #region API
        private void API_ParseRawDataToFrames()
        {
            while (true)
            {
                if (rawData.Count > 0)
                {
                    lock (locker)
                    {

                        int i = rawData.IndexOf((byte)BASIC_FRAME_BYTES.STOP);
                        while (i > -1)
                        {
                            frameData.Enqueue(rawData.GetRange(0, i + 1).ToArray());
                            rawData.RemoveRange(0, i + 1);
                            i = rawData.IndexOf((byte)BASIC_FRAME_BYTES.STOP);
                        }
                        dataReceived = false;
                    }

                }
            }
        }
        public void API_SendReadRequest(byte serv, byte char_number)
        {
            pendingAction = PENDING_FLAGS.PENDING_FOR_DATA;
            pendingService = serv;
            pendingCharacteristic = char_number;
            pendingResponseType = (byte)BASIC_FRAME_BYTES.READ;
            SerialPort_DataWrite(rqRF(pendingService, pendingCharacteristic));
        }
        public void API_FetchForReadResponse(byte serv, byte char_number) //TODO: timeout
        {
            API_SendReadRequest(serv, char_number);
            while (connected && pendingAction != PENDING_FLAGS.PENDING_FOR_NO_ACTION) API_DataExecute(API_DataDecode(pendingAction));
        }
        public void API_SendWriteRequest(byte serv, byte char_number, byte[] data)
        {
            pendingAction = PENDING_FLAGS.PENDING_FOR_DATA;
            pendingService = serv;
            pendingCharacteristic = char_number;
            pendingResponseType = (byte)BASIC_FRAME_BYTES.WRITE;
            SerialPort_DataWrite(rqWF(pendingService, pendingCharacteristic, data));
        }
        public void API_FetchForWriteResponse(byte serv, byte char_number, byte[] data) //TODO: timeout
        {
            API_SendWriteRequest(serv, char_number, data);
            while (connected && pendingAction != PENDING_FLAGS.PENDING_FOR_NO_ACTION) API_DataExecute(API_DataDecode(pendingAction));
        }
        private NOTIFY_FLAGS API_DataDecode(PENDING_FLAGS pendingFor)
        {
            if (frameData.Count > 0)
            {

                if (frameData.TryPeek(out byte[] frame))
                    switch ((int)pendingFor)
                    {
                        case (int)PENDING_FLAGS.PENDING_FOR_CONNECTION:
                            {
                                if (validateConnectionNotify(frame)) return NOTIFY_FLAGS.NOTIFY_CONNECTION;
                                break;
                            }
                        case (int)PENDING_FLAGS.PENDING_FOR_HANDSHAKE:
                            {
                                if (validateHandshakeNotify(frame)) return NOTIFY_FLAGS.NOTIFY_HANDSHAKE;
                                break;
                            }
                        case (int)PENDING_FLAGS.PENDING_FOR_DATA:
                            {
                                if (validateDataFrame(frame)) return NOTIFY_FLAGS.NOTIFY_DATA_RECEIVED;
                                else frameData.TryDequeue(out byte[] tryFrame);


                                break;
                            }
                        default: break;
                    }
            }
            return NOTIFY_FLAGS.NO_NOTIFY;
        }
        private void API_DataExecute(NOTIFY_FLAGS notify)
        {
            switch ((int)notify)
            {
                case (int)NOTIFY_FLAGS.NOTIFY_CONNECTION:
                    {
                        API_ConsumeConnectionData();
                        break;
                    }
                case (int)NOTIFY_FLAGS.NOTIFY_HANDSHAKE:
                    {
                        API_ConsumeHandshakeData();
                        break;
                    }
                case (int)NOTIFY_FLAGS.NOTIFY_DATA_RECEIVED:
                    {


                        if (frameData.TryDequeue(out byte[] frame))
                            API_ConsumeReceivedData(frame);
                        break;
                    }
                case (int)NOTIFY_FLAGS.NO_NOTIFY: break;
                default: break;
            }
        }
        private void API_ConsumeConnectionData()
        {
            SerialPort_DataWrite(START_FRAME);
            serialPort.DiscardInBuffer();
            rawData.Clear();
            frameData.Clear();
            pendingAction = PENDING_FLAGS.PENDING_FOR_HANDSHAKE;
            notifyReceived = NOTIFY_FLAGS.NO_NOTIFY;
        }
        private void API_ConsumeHandshakeData()
        {
            connected = true;
            serialPort.DiscardInBuffer();
            rawData.Clear();
            frameData.Clear();
            pendingAction = PENDING_FLAGS.PENDING_FOR_NO_ACTION;
            notifyReceived = NOTIFY_FLAGS.NO_NOTIFY;
        }
        private void API_ConsumeReceivedData(byte[] frame)
        {
            if (frame != null)
                switch (frame[(int)BASIC_FRAME_OFFSETS.SRV_OFFSET])
                {
                    case (int)SERVICES.BATTERY:
                        API_BATTERY_ConsumeReceivedData(frame);
                        break;
                    case (int)SERVICES.SDCARD:
                        API_SDCARD_ConsumeReceivedData(frame);
                        break;
                    case (int)SERVICES.ADC:
                        API_ADC_ConsumeReceivedData(frame);
                        break;
                    case (int)SERVICES.CUSTOM_DATA:
                        API_CUSTOM_DATA_ConsumeReceivedData(frame);
                        break;
                    default:
                        break;
                }
        }
        private void API_ADC_ConsumeReceivedData(byte[] frame)
        {
            if (validateCharRead(frame) && validateReceivedDataLength(frame))
            {
                switch (frame[(int)BASIC_FRAME_OFFSETS.CHAR_OFFSET])
                {
                    case (int)ADC_CHAR.RUNNING:
                        adcHandler.setRunState(frame[(byte)BASIC_FRAME_OFFSETS.RESPONSE_DATA_OFFSET]);
                        pendingAction = PENDING_FLAGS.PENDING_FOR_NO_ACTION;
                        notifyReceived = NOTIFY_FLAGS.NO_NOTIFY;
                        break;
                    case (int)ADC_CHAR.PAUSE:
                        if (frame[(byte)BASIC_FRAME_OFFSETS.RESPONSE_DATA_OFFSET] == (byte)PAUSE_STATES.PAUSED) adcHandler.pauseStates = PAUSE_STATES.PAUSED;
                        else adcHandler.pauseStates = PAUSE_STATES.NOT_PAUSED;
                        pendingAction = PENDING_FLAGS.PENDING_FOR_NO_ACTION;
                        notifyReceived = NOTIFY_FLAGS.NO_NOTIFY;
                        break;
                    case (int)ADC_CHAR.DATA:
                        DataStorage.dataBaseData.Enqueue(frame);
                        adcHandler.setAdcData(frame);
                        pendingAction = PENDING_FLAGS.PENDING_FOR_DATA;
                        notifyReceived = NOTIFY_FLAGS.NO_NOTIFY;
                        break;
                    case (int)ADC_CHAR.FREQUENCY:
                        adcHandler.setFrequencyFromResponse(frame);
                        pendingAction = PENDING_FLAGS.PENDING_FOR_NO_ACTION;
                        notifyReceived = NOTIFY_FLAGS.NO_NOTIFY;
                        break;
                    case (int)ADC_CHAR.FREQUENCY_AVAILABLE:
                        adcHandler.setAvailableFrequencies(frame);
                        BluetoothAvaiableFreq(adcHandler.FrequenciesAvailable);
                        pendingAction = PENDING_FLAGS.PENDING_FOR_NO_ACTION;
                        notifyReceived = NOTIFY_FLAGS.NO_NOTIFY;
                        break;
                    case (int)ADC_CHAR.CHANNELS_ACTIVE:
                        adcHandler.setChannelsActiveFromResponse(frame);
                        pendingAction = PENDING_FLAGS.PENDING_FOR_NO_ACTION;
                        notifyReceived = NOTIFY_FLAGS.NO_NOTIFY;
                        break;
                    default:
                        pendingAction = PENDING_FLAGS.PENDING_FOR_NO_ACTION;
                        notifyReceived = NOTIFY_FLAGS.NO_NOTIFY;
                        break;
                }
            }
            if (validateCharWrite(frame))
            {
                API_WriteResponseHandler(frame);
                switch (frame[(int)BASIC_FRAME_OFFSETS.CHAR_OFFSET])
                {
                    case (int)ADC_CHAR.RUNNING:
                        if (adcHandler.runStates == ADCLib.RUN_STATES.RUNNING)
                            pendingAction = PENDING_FLAGS.PENDING_FOR_DATA;
                        else pendingAction = PENDING_FLAGS.PENDING_FOR_NO_ACTION;
                        notifyReceived = NOTIFY_FLAGS.NO_NOTIFY;
                        break;
                    case (int)ADC_CHAR.PAUSE:
                        if (adcHandler.pauseStates == ADCLib.PAUSE_STATES.NOT_PAUSED)
                            pendingAction = PENDING_FLAGS.PENDING_FOR_DATA;
                        else pendingAction = PENDING_FLAGS.PENDING_FOR_NO_ACTION;
                        notifyReceived = NOTIFY_FLAGS.NO_NOTIFY;
                        break;
                    case (int)ADC_CHAR.FREQUENCY:
                        pendingAction = PENDING_FLAGS.PENDING_FOR_NO_ACTION;
                        notifyReceived = NOTIFY_FLAGS.NO_NOTIFY;
                        break;
                    case (int)ADC_CHAR.CHANNELS_ACTIVE:
                        pendingAction = PENDING_FLAGS.PENDING_FOR_NO_ACTION;
                        notifyReceived = NOTIFY_FLAGS.NO_NOTIFY;
                        break;
                    default:
                        pendingAction = PENDING_FLAGS.PENDING_FOR_NO_ACTION;
                        notifyReceived = NOTIFY_FLAGS.NO_NOTIFY;
                        break;
                }
            }
        }
        private void API_SDCARD_ConsumeReceivedData(byte[] frame)
        {
            if (validateCharRead(frame) && validateReceivedDataLength(frame))
            {
                switch (frame[(int)BASIC_FRAME_OFFSETS.CHAR_OFFSET])
                {
                    case (int)SDCARD_CHAR.FLOAT_PRECISION:
                        sdCardHandler.setFloatPrecision(frame);
                        break;
                    case (int)SDCARD_CHAR.FORMAT:
                        sdCardHandler.setFormatState(frame);
                        break;
                    case (int)SDCARD_CHAR.OVERFLOW:
                        sdCardHandler.setOverflowState(frame);
                        break;
                    case (int)SDCARD_CHAR.TIMESTAMP:
                        sdCardHandler.setTimeStamp(frame);
                        break;
                    default:
                        break;
                }
            }
            if (validateCharWrite(frame))
            {
                API_WriteResponseHandler(frame);
                switch (frame[(int)BASIC_FRAME_OFFSETS.CHAR_OFFSET])
                {
                    default:
                        pendingAction = PENDING_FLAGS.PENDING_FOR_NO_ACTION;
                        notifyReceived = NOTIFY_FLAGS.NO_NOTIFY;
                        break;
                }
            }
        }
        private void API_BATTERY_ConsumeReceivedData(byte[] frame)
        {
            if (validateCharRead(frame) && validateReceivedDataLength(frame))
            {
                switch (frame[(int)BASIC_FRAME_OFFSETS.CHAR_OFFSET])
                {
                    case (int)BATTERY_CHAR.CHARGE_STATE:
                        batteryHandler.setChargeState(frame);
                        BluetoothBatteryChargeState((int)batteryHandler.battery_power_state);
                        if (manualTriggered == false)
                            pendingAction = PENDING_FLAGS.PENDING_FOR_DATA;
                        else
                        {
                            pendingAction = PENDING_FLAGS.PENDING_FOR_NO_ACTION;
                            manualTriggered = false;
                        }
                        notifyReceived = NOTIFY_FLAGS.NO_NOTIFY;
                        break;
                    case (int)BATTERY_CHAR.POWER_LEVEL:
                        batteryHandler.setPowerLevel(frame);
                        BluetoothBatteryPowerLevel(batteryHandler.battery_level);
                        if (manualTriggered == false)
                            pendingAction = PENDING_FLAGS.PENDING_FOR_DATA;
                        else
                        {
                            pendingAction = PENDING_FLAGS.PENDING_FOR_NO_ACTION;
                            manualTriggered = false;
                        }
                        notifyReceived = NOTIFY_FLAGS.NO_NOTIFY;
                        break;
                    default:
                        break;
                }
            }
            if (validateCharWrite(frame))
            {
                pendingAction = PENDING_FLAGS.PENDING_FOR_NO_ACTION;
                notifyReceived = NOTIFY_FLAGS.NO_NOTIFY;
            }
        }
        private void API_CUSTOM_DATA_ConsumeReceivedData(byte[] frame)
        {
            if (validateCharWrite(frame))
            {
                API_WriteResponseHandler(frame);
                switch (frame[(int)BASIC_FRAME_OFFSETS.CHAR_OFFSET])
                {
                    default:
                        pendingAction = PENDING_FLAGS.PENDING_FOR_NO_ACTION;
                        notifyReceived = NOTIFY_FLAGS.NO_NOTIFY;
                        break;
                }
            }
        }
        private void API_WriteResponseHandler(byte[] frame)
        {
            if (frame[(byte)BASIC_FRAME_OFFSETS.RESPONSE_DATA_OFFSET] == 0)
            {
                string error = $"Failure from write to serv {frame[(int)BASIC_FRAME_OFFSETS.SRV_OFFSET]}, char{frame[(int)BASIC_FRAME_OFFSETS.CHAR_OFFSET]}";
                window.Dispatcher.Invoke(() => window.writeResponseError(error));
            }
        }
        public void API_FetchADCSamples()
        {
            pendingAction = PENDING_FLAGS.PENDING_FOR_DATA;
            while (adcHandler.runStates == RUN_STATES.RUNNING) if (pendingAction != PENDING_FLAGS.PENDING_FOR_NO_ACTION) API_DataExecute(API_DataDecode(pendingAction));
            while (frameData.Count > 0)
            {
                byte[] frame;
                if (frameData.TryDequeue(out frame))
                    if(frame[(int)BASIC_FRAME_OFFSETS.SRV_OFFSET]==(int)SERVICES.ADC)
                    API_ADC_ConsumeReceivedData(frame);
                        
            }
        }
        #endregion

        public void setconName(string conName)
        {
            this.usedComName = conName;
        }

        public void takeAvailableComsName()
        {
            availableComsName = SerialPort.GetPortNames();
            NewPortDiscovered(availableComsName);
        }

        public void StartParseRawDataProcedure() => new Thread(() => API_ParseRawDataToFrames()).Start();
        public void StartInitConnectionProcedure() { if (!running) new Thread(() => InitConnectionProcedure()).Start(); }
        public void StartToggleADCSamplingProcedure(RUN_STATES state, byte frequencyValue) { new Thread(() => ToggleADCSamplingProcedure(state, frequencyValue)).Start(); }
        public void StartTogglePauseProcedure(PAUSE_STATES state) { new Thread(() => TogglePauseProcedure(state)).Start(); }
        public void StartDatabaseSavingProcedure() { new Thread(() => DatabaseSavingProcedure()).Start(); }

        public void Stop()
        {
            serialPort.Close();
            running = running ? false : running;
        }

        public void InitConnectionProcedure() //TOFIX: change name
        {
            running = true;
            this.SerialPort_Open();
            pendingAction = PENDING_FLAGS.PENDING_FOR_CONNECTION;
            StartParseRawDataProcedure();
            while (!connected) if (pendingAction != PENDING_FLAGS.PENDING_FOR_NO_ACTION) API_DataExecute(API_DataDecode(pendingAction));
            BluetoothConnectionHasBeenMade(true);
            manualTriggered = true;
            API_FetchForReadResponse((byte)SERVICES.BATTERY, (byte)BATTERY_CHAR.POWER_LEVEL);
            manualTriggered = true;
            API_FetchForReadResponse((byte)SERVICES.BATTERY, (byte)BATTERY_CHAR.CHARGE_STATE);
            API_FetchForReadResponse((byte)SERVICES.ADC, (byte)ADC_CHAR.RUNNING);
            API_FetchForReadResponse((byte)SERVICES.ADC, (byte)ADC_CHAR.PAUSE);
            API_FetchForReadResponse((byte)SERVICES.ADC, (byte)ADC_CHAR.FREQUENCY_AVAILABLE);
            API_FetchForReadResponse((byte)SERVICES.ADC, (byte)ADC_CHAR.FREQUENCY);
            API_FetchForReadResponse((byte)SERVICES.ADC, (byte)ADC_CHAR.CHANNELS_ACTIVE);
        }

        public void ToggleADCSamplingProcedure(RUN_STATES state, byte frequencyValue)
        {
            if (state == RUN_STATES.RUNNING)
            {
                API_FetchForWriteResponse((byte)COM_Enum.SERVICES.CUSTOM_DATA, (byte)COM_Enum.CUSTOM_DATA_CHAR.DATA_LENGTH, new byte[] { (byte)COM_Enum.DATA_LENGTH_BYTES._8_B });
                API_FetchForWriteResponse((byte)COM_Enum.SERVICES.ADC, (byte)COM_Enum.ADC_CHAR.CHANNELS_ACTIVE, adcHandler.ChannelsList.ToArray<byte>());
                Thread.Sleep(200);
                API_FetchForWriteResponse((byte)COM_Enum.SERVICES.ADC, (byte)COM_Enum.ADC_CHAR.FREQUENCY, new byte[] { frequencyValue });
                DataStorage.CreateTable(adcHandler.ActiveChannelsNumber, adcHandler.ActiveChannelIndexes);

                DataStorage.DatabaseWorkStateFlag = true;
                StartDatabaseSavingProcedure();

                API_FetchForWriteResponse((byte)COM_Enum.SERVICES.ADC, (byte)COM_Enum.ADC_CHAR.RUNNING, new byte[] { (byte)RUN_STATES.RUNNING });
                adcHandler.setRunState((byte)RUN_STATES.RUNNING);
                API_FetchADCSamples();

                DataStorage.DatabaseWorkStateFlag = false;
            }
            else
            {
                adcHandler.setRunState((byte)RUN_STATES.STOPPED);
                API_SendWriteRequest((byte)COM_Enum.SERVICES.ADC, (byte)COM_Enum.ADC_CHAR.RUNNING, new byte[] { (byte)state });
                if (state == (byte)RUN_STATES.STOPPED) while (connected && pendingAction != PENDING_FLAGS.PENDING_FOR_NO_ACTION) ;
            }
        }

        public void TogglePauseProcedure(PAUSE_STATES state)
        {
            adcHandler.pauseStates = state;
            API_SendWriteRequest((byte)COM_Enum.SERVICES.ADC, (byte)COM_Enum.ADC_CHAR.PAUSE, new byte[] { (byte)state });
            if (state == PAUSE_STATES.PAUSED) while (connected && pendingAction != PENDING_FLAGS.PENDING_FOR_NO_ACTION) ;
        }

        public void DatabaseSavingProcedure()
        {
            while (DataStorage.DatabaseWorkStateFlag)
            {
                if (DataStorage.dataBaseData.Count > 100)
                {
                    DataStorage.PopulateTable(adcHandler.ActiveChannelsNumber, adcHandler.ActiveChannelIndexes);
                    DataStorage.ExecuteInsertQuary();
                }
            }

            while (DataStorage.dataBaseData.Count > 0)
            {
                DataStorage.PopulateTable(adcHandler.ActiveChannelsNumber, adcHandler.ActiveChannelIndexes);
                DataStorage.ExecuteInsertQuary();
            }

           
        }
    }
}