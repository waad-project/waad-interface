﻿using System;
using System.Collections.Generic;
using System.Text;
using static WAADInterface.Bluetooth_classes.COMLIB.COM_Enum;

namespace WAADInterface.Bluetooth_classes.COMLIB
{
    class COM_ApiValidator
    {
        public static bool validateConnectionNotify(byte[] frame)
        {
            if (frame == null) return false;
            if (frame.Length < 6) return false;
            for (int i = 0; i < 6; i++) if (frame[i] != START_FRAME[i]) return false;
            return true;
        }

        public static bool validateHandshakeNotify(byte[] frame)
        {
            if (frame == null) return false;
            if (frame.Length < 6) return false;
            for (int i = 0; i < 6; i++) if (frame[i] != HANDSHAKE_FRAME[i]) return false;
            return true;
        }

        public static bool validateDataFrame(byte[] frame)
        {
            if (frame == null) return false;
            if (frame[0] != (byte)BASIC_FRAME_BYTES.START) return false;
            if (frame[frame.Length - 1] != (byte)BASIC_FRAME_BYTES.STOP) return false;
            return true;
        }

        public static bool validateCharRead(byte[] frame) => frame[(byte)BASIC_FRAME_OFFSETS.RW_OFFSET] == (byte)BASIC_FRAME_BYTES.READ;
        public static bool validateCharWrite(byte[] frame) => frame[(byte)BASIC_FRAME_OFFSETS.RW_OFFSET] == (byte)BASIC_FRAME_BYTES.WRITE;
        public static bool validateReceivedDataLength(byte[] frame) => DATA_LENGTH_BYTES_VALUES[frame[(byte)BASIC_FRAME_OFFSETS.LENGTH_OFFSET]] == frame.Length - 6;
    }
}
