﻿using System;
using System.Collections.Generic;
using System.Text;


namespace WAADInterface.Bluetooth_classes.COMLIB
{
    class COM_Enum
    {
        #region

        public enum WRITE_STATUS 
        {
            FAILURE,
            SUCCES
        }
        public enum ACTION_FLAGS : byte
        {
            CONNECTION_RESPONSE
        }
        public enum PENDING_FLAGS : byte
        {
            PENDING_FOR_NO_ACTION,
            PENDING_FOR_CONNECTION,
            PENDING_FOR_HANDSHAKE,
            PENDING_FOR_DATA
        }
        public enum NOTIFY_FLAGS : byte
        {
            NO_NOTIFY,
            GENERAL_NOTIFY,
            NOTIFY_CONNECTION,
            NOTIFY_HANDSHAKE,
            NOTIFY_DATA_RECEIVED
        }
        public enum SERVICES : byte
        {
            
            SDCARD=1,
            ADC=2,
            BATTERY = 3,
            CUSTOM_DATA = 15
        }
        public enum BATTERY_CHAR : byte
        {
            POWER_LEVEL,
            CHARGE_STATE
        }
        public enum SDCARD_CHAR : byte
        {
            FLOAT_PRECISION,
            FORMAT,
            OVERFLOW,
            TIMESTAMP
        }
        public enum ADC_CHAR : byte
        {
            RUNNING,
            PAUSE,
            DATA,
            FREQUENCY,
            FREQUENCY_AVAILABLE,
            CHANNELS_ACTIVE
        }

        public enum CUSTOM_DATA_CHAR : byte
        {
            DATA_LENGTH,
        }

        public enum BASIC_FRAME_BYTES : byte
        {
            EMPTY = 0,
            READ = 0,
            WRITE = 1,
            START = 192,
            STOP = 255
        }

        public enum BASIC_FRAME_OFFSETS : byte
        {
            START_OFFSET = 0,
            SRV_OFFSET = 1,
            CHAR_OFFSET = 2,
            RW_OFFSET = 3,
            LENGTH_OFFSET = 4,
            REQUEST_DATA_OFFSET = 4,
            RESPONSE_DATA_OFFSET = 5,
        }

        public enum DATA_LENGTH_BYTES : byte
        {
            _1_B,
            _2_B,
            _4_B,
            _8_B,
            _16_B,
            _32_B,
            _64_B,
            _128_B,
            _256_B
        }

        public static readonly ushort[] DATA_LENGTH_BYTES_VALUES = {
            1, 2, 4, 8, 16, 32, 64, 128, 256
        };

        public static readonly byte[] START_FRAME = {
            (byte)BASIC_FRAME_BYTES.START,
            0,
            0,
            0,
            0,
            (byte)BASIC_FRAME_BYTES.STOP
        };

        public static readonly byte[] HANDSHAKE_FRAME = {
            (byte)BASIC_FRAME_BYTES.START,
            1,
            1,
            1,
            1,
            (byte)BASIC_FRAME_BYTES.STOP
        };

        public static byte[] rqRF(byte _srv, byte _char)
        {
            return new byte[6] { (byte)BASIC_FRAME_BYTES.START, _srv, _char, (byte)BASIC_FRAME_BYTES.READ, (byte)BASIC_FRAME_BYTES.EMPTY, (byte)BASIC_FRAME_BYTES.STOP };
        }

        public static byte[] rqWF(byte _srv, byte _char, byte[] data)
        {
            byte[] _;
            if (data.Length > 1)
            {
                _ = new byte[data.Length + 5];
                _[_.Length - 1] = (byte)BASIC_FRAME_BYTES.STOP;
                _[(byte)BASIC_FRAME_OFFSETS.START_OFFSET] = (byte)BASIC_FRAME_BYTES.START;
                _[(byte)BASIC_FRAME_OFFSETS.SRV_OFFSET] = _srv;
                _[(byte)BASIC_FRAME_OFFSETS.CHAR_OFFSET] = _char;
                _[(byte)BASIC_FRAME_OFFSETS.RW_OFFSET] = (byte)BASIC_FRAME_BYTES.WRITE;
                Array.Copy(data, 0, _, (byte)BASIC_FRAME_OFFSETS.REQUEST_DATA_OFFSET, data.Length);
                return _;
            }
            else _ = new byte[6] { (byte)BASIC_FRAME_BYTES.START, _srv, _char, (byte)BASIC_FRAME_BYTES.WRITE, data[0], (byte)BASIC_FRAME_BYTES.STOP };
            return _;
        }

        #endregion
    }

}
