﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WAADInterface.Bluetooth_classes.COMLIB
{
    class SDCARDLib // do spytania
    {
        #region enums
        public enum OVERFLOF_STATE
        {
            NOT_OVERFLOW,
            OVERFLOW
        }

        public enum FORMAT_STATE 
        {
            NOT_FORMAT,
            FORMAT
        }
        #endregion
        #region properties
        public OVERFLOF_STATE overflowState;
        public FORMAT_STATE formatState;
        public int floatPrecision;
        public int datasimpleDataValuePosition;
        DateTimeOffset Timestamp; 
        #endregion

        #region Constructor
        public SDCARDLib()
        {
            overflowState = OVERFLOF_STATE.NOT_OVERFLOW;
            formatState = FORMAT_STATE.NOT_FORMAT;
            floatPrecision =0;
            datasimpleDataValuePosition = 5;
            Timestamp = DateTime.Now;
        }
        #endregion
        #region settsers
        internal void setFloatPrecision(byte[] frame)
        {
            floatPrecision = frame[datasimpleDataValuePosition];
        }

        internal void setOverflowState(byte[] frame)
        {
            if (frame[datasimpleDataValuePosition] == 1) {
                overflowState = OVERFLOF_STATE.OVERFLOW;
            }
            else {
                overflowState = OVERFLOF_STATE.NOT_OVERFLOW;
            }
        }

        internal void setFormatState(byte[] frame)
        {
            if (frame[datasimpleDataValuePosition] == 1)
            {
                formatState = FORMAT_STATE.FORMAT;
            }
            else
            {
                formatState = FORMAT_STATE.NOT_FORMAT;
            }
        }
        /// <summary>
        /// ????
        /// </summary>
        /// <param name="frame"></param>
        internal void setTimeStamp(byte [] frame) { 
        
        }
        #endregion






    }
}
