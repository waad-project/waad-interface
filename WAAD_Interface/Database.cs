﻿using Microsoft.Data.Sqlite;

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;

using WAADInterface.Interfaces;
using static WAADInterface.Bluetooth_classes.COMLIB.COM_Enum;


namespace WAADInterface
{
    internal class Database : IDataStorage
    {
        private string ConnectionString;
        private SqliteConnection Connection;
        string tableName;
        public ConcurrentQueue<byte[]> dataBaseData; // new buffer
        public bool DatabaseWorkStateFlag;
        int dataLength;
        String quaryInsert;
        StringBuilder quaryValues;
        SqliteCommand executingCommand;
        internal Database(string filename)
        {
            SqliteConnectionStringBuilder scsb = new SqliteConnectionStringBuilder
            {
                DataSource = Path.GetFullPath(filename)
            };

            ConnectionString = scsb.ConnectionString;
            DatabaseWorkStateFlag = false;
            dataBaseData = new ConcurrentQueue<byte[]>();
            dataLength = 128;
            quaryValues = new StringBuilder();

        }

        public void EndSession()
        {
            throw new NotImplementedException();
        }

        public void StartSession()
        {

        }

        public void StoreDataPoint(double time, double value)
        {


        }

        public void ExecuteInsertQuary()
        {
            executingCommand.CommandText = quaryInsert + quaryValues.ToString().Remove(quaryValues.Length - 2) + ";";
            executingCommand.ExecuteNonQuery();
            executingCommand = Connection.CreateCommand();
            quaryValues.Clear();
        }

        public void CreateTable(int selectedChanelsCount, List<int> ActiveChannelIndexes)
        {
            String timeStamp = GetTimestamp(DateTime.Now);
            tableName = "T" + timeStamp;
            Connection.Open();
            SqliteCommand command = Connection.CreateCommand();

            // $"CREATE TABLE  {tableName}  (id INTEGER PRIMARY KEY AUTOINCREMENT, {string.Join(", ", ActiveChannelIndexes.Select(x => $"channel{x} INTEGER NOT NULL"))} );";
            command.CommandText = $"CREATE TABLE `{tableName}` (id INTEGER PRIMARY KEY AUTOINCREMENT, {string.Join(",", ActiveChannelIndexes.Select(x => $"`channel{x}` INTEGER NOT NULL"))} );";
            command.ExecuteNonQuery();

            quaryInsert = $"INSERT INTO {tableName} ({string.Join(",", ActiveChannelIndexes.Select(x => $"channel{x}"))}) VALUES ";


        }



        public void PopulateTable(int selectedChanelsCount, List<int> ActiveChannelIndexes)
        {

            for (int counter = 0; counter < 100; ++counter)
            {
                byte[] frame;


                if (dataBaseData.TryDequeue(out frame))
                {

                    int total = dataLength / selectedChanelsCount;
                    total *= selectedChanelsCount;
                    int[] buffer = new int[total];
                    for (int i = 0; i < buffer.Length; i++) buffer[i] = frame[i * 2 + (byte)BASIC_FRAME_OFFSETS.RESPONSE_DATA_OFFSET] * 64 + frame[i * 2 + 1 + (byte)BASIC_FRAME_OFFSETS.RESPONSE_DATA_OFFSET];

                    for (int i = 0, j = 0; i < buffer.Length;)
                    {
                        quaryValues.Append("(");
                        int[] tmpBuffer = new int[selectedChanelsCount];
                        Array.Copy(buffer, j * selectedChanelsCount, tmpBuffer, 0, selectedChanelsCount);
                        i += selectedChanelsCount;
                        j++;
                        quaryValues.AppendJoin(",", tmpBuffer.Select(x => x.ToString()));
                        quaryValues.Append("), ");
                    }
                }
                else break;
            }
        }

        public static String GetTimestamp(DateTime value)
        {
            return value.ToString("yyyyMMddHHmmssffff");
        }

        public void Initialize()
        {

            Connection = new SqliteConnection(ConnectionString);

            try
            {

                Connection.Open();
                executingCommand = Connection.CreateCommand();

            }
            catch (Exception e)
            {
                MessageBox.Show($"Wystąpił błąd tworzenia połączenia bazodanowego:\n{e}", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                Connection.Close();
                Connection = null;
            }
        }






    }
}
