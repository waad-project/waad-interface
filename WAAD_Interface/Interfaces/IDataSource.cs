﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WAADInterface.Interfaces
{
    interface IDataSource<T>
    {
        bool RetrieveData(out T value);
        void WipeData();
    }
}
