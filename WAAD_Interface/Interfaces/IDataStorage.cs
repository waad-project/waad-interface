﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WAADInterface.Interfaces
{
    interface IDataStorage
    {
        void StartSession();

        void EndSession();

        void StoreDataPoint(double time, double value);
    }
}
