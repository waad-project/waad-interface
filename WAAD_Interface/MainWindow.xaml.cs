﻿using ScottPlot;
using ScottPlot.Drawing;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using WAADInterface.Bluetooth_classes.COMLIB;
using static WAADInterface.Bluetooth_classes.COMLIB.ADCLib;

namespace WAADInterface
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>


    public partial class MainWindow : Window
    {
        public bool Paused { get; private set; }

        public bool RunningADC { get; private set; }

        public bool RunningThread { get; private set; }

        public int RunState { get; private set; }
        public bool Search { get; private set; }
        public bool IsConnected { get; private set; }
        
        readonly Collection<WpfPlot> mPlots;

        readonly List<PlottableSignal> mPlotSignal;
        readonly List<int> mPlotCount;
        readonly List<double[]> mPlotViewpoint;

        Color[] arrayColor;

        COM_ApiHandler mCOM_ApiHandler;


        string usedComName;

        public byte frequencyTaken;

       // public Thread DatabaseThread;


        public object locker;

        public enum CHANNEL_STATUS
        {
            NOT_SELECTED,
            SELECTED

        };

        public byte[] selectedChanels;
        public MainWindow()
        {
            InitializeComponent();

            selectedChanels = new byte[8];

            DataContext = this;


            mCOM_ApiHandler = new COM_ApiHandler(this);

            mPlots = new Collection<WpfPlot>();

            mPlotSignal = new List<PlottableSignal>();

            mPlotCount = new List<int>(8);

            mPlotViewpoint = new List<double[]>();

            

             locker = new object();

            frequencyTaken = 0;

            RunState = 0;

            usedComName = "";

            Search = false;

            IsConnected = false;

            RunningADC = false;

            collectPlots();

            InitPlot();


            RenderTimer = new DispatcherTimer()
            {
                Interval = TimeSpan.FromMilliseconds(50),
            };
            RenderTimer.Tick += Render;


        }


        private void Plot_AxisChanged(object sender, EventArgs e)
        {
            Plot.plt.Axis(null, null, 0, 4096);
        }

        private void InitPlot()
        {
            foreach (var plot in mPlots)
            {
                plot.plt.YLabel("Wartości");
                plot.plt.XLabel("Czas");
                plot.plt.Style(ScottPlot.Style.Gray2);
                plot.plt.Colorset(Colorset.OneHalfDark);
                plot.plt.Axis(null, null, 0, 4096);
                plot.plt.AxisZoom(0.01, 1);
                plot.AxisChanged += Plot_AxisChanged;
                plot.Visibility = Visibility.Collapsed;

            }

        }

        private void ResetPlot()
        {

            foreach (var Plot in mPlots)
            {
                var plotIndex = mPlots.IndexOf(Plot);
                Array.Clear(mPlotViewpoint[plotIndex], 0, mPlotViewpoint[plotIndex].Length);

                Plot.plt.Clear();
                Plot.plt.Axis(null, null, 0, 4096);
                Plot.plt.AxisZoom(0.01, 1);
                mPlotSignal[plotIndex] = Plot.plt.PlotSignal(mPlotViewpoint[plotIndex],color:arrayColor[plotIndex]);
                mPlotCount[plotIndex] = 0;
            }

        }

       
        internal void Render(object _, EventArgs __)
        {
            foreach (var Plot in mPlots)
                Plot.Render(true);
        }

        public Func<double, string> YFormatter { get; set; }

        public Func<double, string> XFormatter { get; set; }


        internal DispatcherTimer RenderTimer;

        internal DispatcherTimer DataSetTimer;

        internal double[] Viewpoint = new double[500000];

        internal int Count = 0;

        internal PlottableSignal Signal;


        internal void AddPoint(double ptr, int plotIndex)
        {
            if (mPlotCount[plotIndex] >= mPlotViewpoint[plotIndex].Length)
            {
                double[] new_arr = new double[mPlotViewpoint[plotIndex].Length * 2];

                Array.Copy(mPlotViewpoint[plotIndex], new_arr, mPlotViewpoint[plotIndex].Length);

                mPlotViewpoint[plotIndex] = new_arr;

                mPlotSignal[plotIndex] = mPlots[plotIndex].plt.PlotSignal(mPlotViewpoint[plotIndex],color:arrayColor[plotIndex]);
            }

            mPlotViewpoint[plotIndex][mPlotCount[plotIndex]] = ptr;
            mPlotSignal[plotIndex].maxRenderIndex = mPlotCount[plotIndex];
            mPlotCount[plotIndex]++;

            mPlots[plotIndex].plt.AxisPan(1, 0);
           
        }

        internal void collectPlots()
        {
            arrayColor = new Color[] { Color.Red, Color.Blue, Color.Green, Color.Cyan, Color.Magenta, Color.Gold, Color.Silver, Color.PapayaWhip };
            mPlots.Add(Plot);
            mPlots.Add(Plot_Copy);
            mPlots.Add(Plot_Copy1);
            mPlots.Add(Plot_Copy2);
            mPlots.Add(Plot_Copy3);
            mPlots.Add(Plot_Copy4);
            mPlots.Add(Plot_Copy5);
            mPlots.Add(Plot_Copy6);
            for (int i = 0; i < 8; ++i)
            {
                mPlotViewpoint.Add(new double[2000]);
                mPlotSignal.Add(Signal);
                mPlotSignal[i] = mPlots[i].plt.PlotSignal(mPlotViewpoint[i], color: arrayColor[i]);
                mPlotCount.Add(0);
                mPlots[i].plt.AxisAutoY(0.01, false);

            }
            for (int i = 0; i < 8; ++i)
            {

                mPlotSignal[i] = mPlots[i].plt.PlotSignal(mPlotViewpoint[i], color: arrayColor[i]);


            }
        }

            internal void createItemInBox(string[] comsName)
        {
            foreach (var name in comsName)
            {
                DevicesBox.Items.Add(name);
            }
            if(DevicesBox.Items.Count>0)
            Connect.IsEnabled = true;

        }

        internal void addAvHz(List<int> ListAvHZ)
        {

            foreach (var hz in ListAvHZ)
            {

                frequency.Items.Add(hz);
            }
        }

        internal void updateBatteryLevel(int batteryPowerLevelValue)
        {
            B_PowerLevel.Text = $"Stan mocy baterii wynosi: {batteryPowerLevelValue}%";
        }

        internal void updateBatteryChargeState(int batteryChargeState)
        {
            if (batteryChargeState == 1) B_ChargeState.Text = $"Stan ładowania baterii: ładuje się";
            else B_ChargeState.Text = $"Stan ładowania baterii: nie podłączona do ładowarki";
        }

        #region Events

        private void Plot_Loaded(object sender, RoutedEventArgs e)
        {

        }



        public void informAboutPortConnection(bool obj)
        {
            if (obj == true)
                C_Status.Text = $"Połączono z portem {usedComName} ";
            else
            {
                C_Status.Text = "Połączenie zakończono porażką";
            }
        }

        public void informAboutBluetoothConnection(bool obj)
        {
            if (obj == true)
            {
                C_Status.Text = $"Połączono z bluetooth przez port {usedComName} ";
                StartEndButton.IsEnabled = true;
                listBox_.IsEnabled = true;
            }
            else
            {
                C_Status.Text = "Połączenie bluetooth zakończono porażką";
                StartEndButton.IsEnabled = false;
            }
        }


        private void TogglePauseButtonClicked(object sender, RoutedEventArgs e)
        {
            Button b = (Button)sender;
            if (mCOM_ApiHandler.adcHandler.pauseStates == PAUSE_STATES.NOT_PAUSED)
            {
                unlockButtonProcedure(TogglePauseButton);
                mCOM_ApiHandler.StartTogglePauseProcedure(PAUSE_STATES.PAUSED);
                b.Content = "Wznów pomiar";
                
            }
            else
            {
                unlockButtonProcedure(TogglePauseButton);
                mCOM_ApiHandler.StartTogglePauseProcedure(PAUSE_STATES.NOT_PAUSED);
                b.Content = "Wstrzymaj pomiar";
           
            }
        }

        private void ToggleStartEndButtonClicked(object sender, RoutedEventArgs e)
        {

            Button b = (Button)sender;
            if (RunningADC)
            { 
                unlockButtonProcedure(StartEndButton);
                Paused = false;
                RunningADC = false;
                TogglePauseButton.Visibility = Visibility.Collapsed;
                b.Content = "Rozpocznij pomiar";
                StartEndButton.IsEnabled=false;
                mCOM_ApiHandler.StartToggleADCSamplingProcedure(RUN_STATES.STOPPED, mCOM_ApiHandler.adcHandler.freqValue);
                RenderTimer.Stop();
                Disconnect.IsEnabled = true;


            }
            else
            {   
                ResetPlot();
                unlockButtonProcedure(StartEndButton);
                RunningADC = true;
                Paused = false;
                TogglePauseButton.Visibility = Visibility.Visible;
                TogglePauseButton.Content = "Wstrzymaj pomiar";
                b.Content = "Zakończ pomiar";
                mCOM_ApiHandler.adcHandler.setFrequencyFromGUI(frequencyTaken);
                mCOM_ApiHandler.adcHandler.setChannelsActiveFromGUI(selectedChanels);
                mCOM_ApiHandler.StartToggleADCSamplingProcedure(RUN_STATES.RUNNING, mCOM_ApiHandler.adcHandler.freqValue);
                RenderTimer.Start();
                Disconnect.IsEnabled = false;
            }
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (RunningADC)
            {
                Paused = true;
                TogglePauseButton.Content = "Wznów pomiar";
                MessageBoxResult mBoXResult = MessageBox.Show("Trwa pomiar, wyjście z programu wymusi zakończenie programu.", "Wyjście z programu", MessageBoxButton.OKCancel);
                if (mBoXResult == MessageBoxResult.OK) return;
                else e.Cancel = true;
                return;
            }
        }



        private void checkBox_Checked(object sender, RoutedEventArgs e)
        {
            if (listBox_.SelectedIndex != -1)
            {

                if (mPlots[listBox_.SelectedIndex].Visibility != Visibility.Visible)
                {
                    mPlots[listBox_.SelectedIndex].Visibility = Visibility.Visible;


                    selectedChanels[listBox_.SelectedIndex] = (byte)CHANNEL_STATUS.SELECTED;


                }
                else
                {
                    mPlots[listBox_.SelectedIndex].Visibility = Visibility.Collapsed;

                    selectedChanels[listBox_.SelectedIndex] = (byte)CHANNEL_STATUS.NOT_SELECTED;


                }
            }



        }





        #endregion

        private void Connectivity_Click(object sender, RoutedEventArgs e)
        {
            C_Status.Visibility = Visibility.Visible;

            MessageBoxResult mBoXResult = MessageBox.Show($"Czy chcesz połączyć się z portem {usedComName} ", "Informacja o łączeniu", MessageBoxButton.OKCancel);
            if (mBoXResult == MessageBoxResult.OK)
            {
                mCOM_ApiHandler.setconName(usedComName);
                frequency.Items.Clear();
                mCOM_ApiHandler.StartInitConnectionProcedure();
                IsConnected = true;
                Disconnect.IsEnabled = true;
                Connect.IsEnabled = false;
                Connectivity.IsEnabled = false;
            }
            else
            {
                C_Status.Text = "Próbę połączenia przerwano";
            }


        }


        private void Searching_Click(object sender, RoutedEventArgs e)
        {
            if (!Search)
            {
                DevicesBox.Items.Clear();
                mCOM_ApiHandler.takeAvailableComsName();
                Connectivity.Content = "Zakończ szukanie";
                Search = true;
            }
            else
            {

                Connectivity.Content = "Wyszukaj urządzenia";
                Search = false;
            }

        }


        private void DevicesBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DevicesBox.SelectedItem != null)
                usedComName = DevicesBox.SelectedItem.ToString();

        }

        private void frequency_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (frequency.SelectedItem != null)
            {
                frequencyTaken = (byte)frequency.Items.IndexOf(frequency.SelectedItem);
                
            }
                


        }


        private void Disconnect_Click(object sender, RoutedEventArgs e)
        {
            if (IsConnected)
            {
                IsConnected = false;
                mCOM_ApiHandler.Stop();
                mCOM_ApiHandler.connected = false;
                C_Status.Text = "Rozłączono";
                Disconnect.IsEnabled = false;
                Connect.IsEnabled = false;
                StartEndButton.IsEnabled = false;
                listBox_.IsEnabled = false;
                ResetPlot();
                Connectivity.IsEnabled = true;
                mCOM_ApiHandler = new COM_ApiHandler(this);
            }


        }
        internal void writeResponseError(string error)
        {
            Error_Display.Text = error;
        }

        public void unlockButtonProcedure(Button button) { 
            new Thread(() => unlockButton(button)).Start();
    }
        public void unlockButton(Button button) {
            this.Dispatcher.Invoke(() => button.IsEnabled = false);
            Thread.Sleep(4000);
            this.Dispatcher.Invoke(() => button.IsEnabled = true);
        
        }
    }

}
